const valida_idade = function (valor) {
   window.location.href = `home.html?acesso=${valor}`
}

const validar_entrada = function () {
   const url = new URLSearchParams(window.location.search)
   let acesso = url.get("acesso")

   if (acesso==1) {
      document.getElementById("textoEntrada")
          .innerHTML = "Olá! Gostaria de te dar boas vindas a Cervejaria do Pericles.<br>" +
                       "Aqui você encontrará as melhores bebidas do Brasil";
   } else {
      document.getElementById("textoEntrada")
          .innerHTML = "Sinto muito, mas este site só é permitido para maiores de 18 anos! :(";

      document.getElementById("btnEntrarSite")
          .style.visibility = 'hidden'
   }
}